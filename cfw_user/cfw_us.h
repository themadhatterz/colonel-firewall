#ifndef __CFW_US_H__
#define __CFW_US_H__


#define IOCTL_SAY_HI _IO(202, 0)
#define IOCTL_CONN_TABLE _IO(202, 1)
#define IOCTL_DEBUG _IO(202, 2)

#define DBG_ERROR 1U << 0
#define DBG_INFO  1U << 1
#define DBG_CONN  1U << 2
#define DBG_TABLE 1U << 3
#define DBG_INFRA 1U << 4


struct connection {
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
    unsigned char proto;
    struct connection *next;
};

int main(int argc, char **argv);
int print_connection_table(void);
int update_debug_flags(char *str_flag);
int say_hi_to_kernel(void);

#endif //__CFW_US_H__