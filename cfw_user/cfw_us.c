#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>

#include "cfw_us.h"


struct mhfw_rule {
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
    unsigned char proto;
    int action;
};


int
print_connection_table(void)
{
    int fd, rc;

    fd = open("/proc/cfw_kern", 0);
    if (fd < 0)
    {
        printf("Failed to open device file.\n");
        return -1;
    }

    rc = ioctl(fd, IOCTL_CONN_TABLE);
    if (rc < 0)
    {
        printf("ioctl failed!\n");
        close(fd);
        return -1;
    }

    close(fd);
    return 1;
}


int
say_hi_to_kernel(void)
{
    int fd, rc;
    char buffer[255] = "send to kernel";

    fd = open("/proc/cfw_kern", 0);
    if (fd < 0)
    {
        printf("Failed to open device file.\n");
        return -1;
    }

    rc = ioctl(fd, IOCTL_SAY_HI, &buffer);
    if (rc < 0)
    {
        printf("ioctl failed!\n");
        close(fd);
        return -1;
    }
    printf("Message from kernel %s\n", buffer);

    close(fd);
    return 1;
}


int
update_debug_flag(char *str_flag)
{
    int fd, rc;
    unsigned int flag = 0;

    if (strcmp(str_flag, "CONN") == 0)
    {
        flag |= DBG_CONN;
    }
    else if (strcmp(str_flag, "ERROR") == 0)
    {
        flag |= DBG_ERROR;
    }
    else if (strcmp(str_flag, "INFO") == 0)
    {
        flag |= DBG_INFO;
    }
    else if (strcmp(str_flag, "TABLE") == 0)
    {
        flag |= DBG_TABLE;
    }
    else if (strcmp(str_flag, "INFRA") == 0)
    {
        flag |= DBG_INFRA;
    }
    else
    {
        printf("Bad debug flag given.\n");
        return -1;
    }

    fd = open("/proc/cfw_kern", 0);
    if (fd < 0)
    {
        printf("Failed to open device file.\n");
        return -1;
    }

    rc = ioctl(fd, IOCTL_DEBUG, &flag);
    if (rc < 0)
    {
        printf("ioctl failed!\n");
        close(fd);
        return -1;
    }

    printf("Sent flag to kernel.\n");
    close(fd);
    return 1;
}


int
main(int argc, char **argv)
{
    int opt;
    char *arg_value = NULL;

    while ((opt = getopt(argc, argv, "pkrd:")) != -1)
    {
        switch (opt)
        {
            case 'p':
                print_connection_table();
                break;
            case 'k':
                say_hi_to_kernel();
                break;
            case 'd':
                arg_value = optarg;
                update_debug_flag(arg_value);
                break;
            default:
                break;
        }
    }

    return 0;
}