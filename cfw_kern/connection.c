#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ip.h>
#include <linux/tcp.h>

#include "connection.h"
#include "infra.h"
#include "table.h"


struct hash_table *connections_tbl = NULL;


int
init_connections_table(void)
{
    connections_tbl = table_create(CONNECTIONS_TABLE_SIZE);
    if (!connections_tbl)
    {
        cfw_debug(DBG_ERROR, "connection table creation failed.");
        return 0;
    }
    return 1;
}


void
delete_connections_table(void)
{
    table_destroy(connections_tbl);
}


int
is_first_packet(struct tcphdr *tcp_header)
{
    if (tcp_header->syn && !tcp_header->ack)
    {
        return 1;
    }

    return 0;
}


int
is_existing_conn(struct iphdr *ip_header, u16 source_port, u16 destination_port)
{
    char conn_key[50];
    char conn_key_reversed[50];
    struct cfw_connection *cfw_conn = NULL;

    sprintf(conn_key, "%d,%pI4,%d,%pI4,%d", ip_header->protocol, &ip_header->saddr, source_port, &ip_header->daddr, destination_port);
    sprintf(conn_key_reversed, "%d,%pI4,%d,%pI4,%d", ip_header->protocol, &ip_header->daddr, destination_port, &ip_header->saddr, source_port);

    cfw_conn = table_get(connections_tbl, conn_key);
    if (cfw_conn)
    {
        cfw_debug(DBG_CONN, "existing connection found for conn_key=%s", conn_key);
        return 1;
    }
    cfw_debug(DBG_CONN, "existing connection miss for conn_key=%s", conn_key);

    cfw_conn = table_get(connections_tbl, conn_key_reversed);
    if (cfw_conn)
    {
        cfw_debug(DBG_CONN, "existing connection found for conn_key_reversed=%s", conn_key_reversed);
        return 1;
    }
    cfw_debug(DBG_CONN, "existing connection miss for conn_key_reversed=%s", conn_key_reversed);

    return 0;
}


int
create_cfw_conn(struct iphdr *ip_header, u16 source_port, u16 destination_port)
{
    char conn_key[50];
    struct cfw_connection *cfw_conn = cfw_kmalloc(sizeof(struct cfw_connection));

    sprintf(conn_key, "%d,%pI4,%d,%pI4,%d", ip_header->protocol, &ip_header->saddr, source_port, &ip_header->daddr, destination_port);
    cfw_conn->saddr = ip_header->saddr;
    cfw_conn->daddr = ip_header->daddr;
    cfw_conn->sport = source_port;
    cfw_conn->dport = destination_port;
    cfw_conn->proto = ip_header->protocol;

    if (!table_set(connections_tbl, conn_key, cfw_conn))
    {
        cfw_debug(DBG_ERROR, "failed to save connection to connections table.");
        return 0;
    }

    return 1;
}


void print_connections_table(void)
{
    int i = 0;
    struct entry *tbl_entry = NULL;
    struct cfw_connection *cfw_conn = NULL;

    for (i = 0; i < connections_tbl->size; i++)
    {
        tbl_entry = connections_tbl->entries[i];
        if (tbl_entry)
        {
            cfw_conn = (struct cfw_connection *)(tbl_entry->value);
            cfw_debug(DBG_INFO, "CONNECTION[%d] > %d,%pI4,%d,%pI4,%d", i, cfw_conn->proto, &cfw_conn->saddr, cfw_conn->sport, &cfw_conn->daddr, cfw_conn->dport);
        }
    }
}