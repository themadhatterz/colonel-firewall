#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/ioctl.h>

#include "cfwdrv.h"
#include "connection.h"
#include "infra.h"


MODULE_LICENSE("PRIVATE");
MODULE_AUTHOR("Colonel Firewall Team");
MODULE_DESCRIPTION("Colonel Firewall");
MODULE_VERSION("0.0.1");


// Colonel Firewall Packet Handling
unsigned int
cfw_handle_packet(struct sk_buff *skb)
{
    struct iphdr *ip_header = NULL;
    struct tcphdr *tcp_header = NULL;
    struct udphdr *udp_header = NULL;
    u16 source_port, destination_port;

    if (!skb)
    {
        cfw_debug(DBG_ERROR, "received empty SKB.");
        return 0;
    }

    ip_header = ip_hdr(skb);

    if (ip_header->protocol == IPPROTO_TCP)
    {
        tcp_header = tcp_hdr(skb);
        source_port = ntohs(tcp_header->source);
        destination_port = ntohs(tcp_header->dest);
    }
    else if(ip_header->protocol == IPPROTO_UDP)
    {
        udp_header = udp_hdr(skb);
        source_port = ntohs(udp_header->source);
        destination_port = ntohs(udp_header->dest);
    }
    else
    {
        cfw_debug(DBG_CONN, "Unhandled Protocol %d", ip_header->protocol);
        return 1;
    }

    cfw_debug(DBG_CONN, "%d %pI4:%d -> %pI4:%d", ip_header->protocol, &ip_header->saddr, source_port, &ip_header->daddr, destination_port);
    if (ip_header->protocol == IPPROTO_TCP && is_first_packet(tcp_header))
    {
        cfw_debug(DBG_INFO, "SYN %d %pI4:%d -> %pI4:%d", ip_header->protocol, &ip_header->saddr, source_port, &ip_header->daddr, destination_port);
        if (!create_cfw_conn(ip_header, source_port, destination_port))
        {
            cfw_debug(DBG_ERROR, "Failed to create tcp cfw connection");
            return 0;
        }
        return 1;
    }
    else if(is_existing_conn(ip_header, source_port, destination_port))
    {
        cfw_debug(DBG_CONN, "found in connections table");
        return 1;
    }
    else if(ip_header->protocol == IPPROTO_UDP)
    {
        cfw_debug(DBG_INFO, "NEW %d %pI4:%d -> %pI4:%d", ip_header->protocol, &ip_header->saddr, source_port, &ip_header->daddr, destination_port);
        if (!create_cfw_conn(ip_header, source_port, destination_port))
        {
            cfw_debug(DBG_ERROR, "Failed to create udp cfw connection");
            return 0;
        }
        return 1;
    }

#ifdef ALLOW
    cfw_debug(DBG_CONN, "ALLOW ALL DEBUG KERNEL!!!");
    return 1;
#endif

    return 0;
}


// Inbound Colonel Firewall Handling
unsigned int
cfw_inbound(void *private, struct sk_buff *skb, const struct nf_hook_state *state)
{
    cfw_debug(DBG_CONN, "[ CFW INBOUND ENTER ]");
    if (cfw_handle_packet(skb))
    {
        cfw_debug(DBG_CONN, "[ CFW INBOUND ACCEPT ]");
        return NF_ACCEPT;
    }
    cfw_debug(DBG_CONN, "[ CFW INBOUND DROP ]");
    return NF_DROP;
}


// Outbound Colonel Firewall Handling
unsigned int
cfw_outbound(void *private, struct sk_buff *skb, const struct nf_hook_state *state)
{
    cfw_debug(DBG_CONN, "[ CFW OUTBOUND ENTER ]");
    if (cfw_handle_packet(skb))
    {
        cfw_debug(DBG_CONN, "[ CFW OUTBOUND ACCEPT ]");
        return NF_ACCEPT;
    }
    cfw_debug(DBG_CONN, "[ CFW OUTBOUND DROP ]");
    return NF_DROP;
}


// Forward Colonel Firewall Handling
unsigned int
cfw_forward(void *private, struct sk_buff *skb, const struct nf_hook_state *state)
{
    cfw_debug(DBG_CONN, "[ CFW FORWARD ENTER ]");
    if (cfw_handle_packet(skb))
    {
        cfw_debug(DBG_CONN, "[ CFW FORWARD ACCEPT ]");
        return NF_ACCEPT;
    }
    cfw_debug(DBG_CONN, "[ CFW FORWARD DROP ]");
    return NF_DROP;
}



// Colonel Firewall Read Function
// Currently no read functionality implemented
static ssize_t
cfw_read(struct file *fp, char *buffer, size_t len, loff_t *offset)
{
    return 0;
}



// Colonel Firewall Write Function
// Currently no write functionality implemented
static ssize_t
cfw_write(struct file *fp, const char *buffer, size_t len, loff_t *offset)
{
    return 0;
}



// Colonel Firewall IOCTL
// Called for any IOCTL received. Actions according to IOCTL number
long
cfw_ioctl(struct file *fp, unsigned int ioctl_num, unsigned long ioctl_param)
{
    int rc = 0;
    unsigned int dbg_flag = 0;
    char user_message[255];
    char kern_message[255] = "Colonel Firewall!";

    switch (ioctl_num)
    {
        // Example IOCTL
        case IOCTL_SAY_HI:
            rc = copy_from_user(user_message, (char *)ioctl_param, sizeof(user_message));
            if (rc == 0)
            {
                cfw_debug(DBG_INFO, "Received message from Userspace");
                cfw_debug(DBG_INFO, "%s", user_message);
            }
            rc = copy_to_user((char *)ioctl_param, kern_message, sizeof(kern_message));
            if (rc == 0)
            {
                cfw_debug(DBG_INFO, "Sent message to Userspace");
            }
            break;

        case IOCTL_CONN_TABLE:
            cfw_debug(DBG_INFO, "Connection Table Request");
            print_connections_table();
            break;

        case IOCTL_DEBUG:
            rc = copy_from_user(&dbg_flag, (unsigned int *)ioctl_param, sizeof(unsigned int));
            if (rc == 0)
            {
                cfw_debug(DBG_INFO, "Updating Kernel debug flag=%u", dbg_flag);
                update_debug_flags(dbg_flag);
            }
            break;

        default:
            cfw_debug(DBG_ERROR, "Invalid IOCTL Command: #%u", ioctl_num);
            break;
    }

    return 0;
}


// Netfilter Registration
// Links netfilter to defined function in CFW Code
// Called when kernel module is loaded
int
register_netfilter_hooks(void)
{
    nfho_localin.hook = cfw_inbound;
    nfho_localin.hooknum = NF_INET_LOCAL_IN;
    nfho_localin.pf = PF_INET;
    nfho_localin.priority = NF_IP_PRI_FIRST;
    nf_register_net_hook(&init_net, &nfho_localin);

    nfho_preroute.hook = cfw_inbound;
    nfho_preroute.hooknum = NF_INET_PRE_ROUTING;
    nfho_preroute.pf = PF_INET;
    nfho_preroute.priority = NF_IP_PRI_FIRST;
    nf_register_net_hook(&init_net, &nfho_preroute);

    nfho_localout.hook = cfw_outbound;
    nfho_localout.hooknum = NF_INET_LOCAL_OUT;
    nfho_localout.pf = PF_INET;
    nfho_localout.priority = NF_IP_PRI_FIRST;
    nf_register_net_hook(&init_net, &nfho_localout);

    nfho_postroute.hook = cfw_outbound;
    nfho_postroute.hooknum = NF_INET_POST_ROUTING;
    nfho_postroute.pf = PF_INET;
    nfho_postroute.priority = NF_IP_PRI_FIRST;
    nf_register_net_hook(&init_net, &nfho_postroute);

    nfho_forward.hook = cfw_forward;
    nfho_forward.hooknum = NF_INET_FORWARD;
    nfho_forward.pf = PF_INET;
    nfho_forward.priority = NF_IP_PRI_FIRST;
    nf_register_net_hook(&init_net, &nfho_forward);

    cfw_debug(DBG_INFO, "Netfilter hooks registered");
    return 1;
}


// Unregister for Netfilter hooks
// Called when kernel unsigned odule is unloaded
void
unregister_netfilter_hooks(void)
{
    nf_unregister_net_hook(&init_net, &nfho_localin);
    nf_unregister_net_hook(&init_net, &nfho_localout);
    nf_unregister_net_hook(&init_net, &nfho_preroute);
    nf_unregister_net_hook(&init_net, &nfho_postroute);
    nf_unregister_net_hook(&init_net, &nfho_forward);
}


// Register process basic functions
int
register_process(void)
{
    int rc = 1;
    static const struct file_operations proc_file_fops = {
        .owner = THIS_MODULE,
        .read = cfw_read,
        .write = cfw_write,
        .unlocked_ioctl = cfw_ioctl,
    };
    process_entry = proc_create(PROCESS_NAME, PROCESS_PERM, NULL, &proc_file_fops);
    if (!process_entry)
    {
        rc = -ENOMEM;
        cfw_debug(DBG_ERROR, "Could not create process entry!!!");
    }
    else
    {
        cfw_debug(DBG_INFO, "Process entry created");
    }

    return rc;
}


// Unregister process basic function
void
unregister_process(void)
{
    remove_proc_entry(PROCESS_NAME, NULL);
    cfw_debug(DBG_INFO, "Process entry removed");
}


// First call when kernel module is loaded
static int
__init cfw_init(void)
{
    register_process();
    register_netfilter_hooks();
    init_connections_table();
    cfw_debug(DBG_INFO, "Colonel Firewall Loaded");
    return 0;
}


// First call when kernel module is unloaded
static void
__exit cfw_exit(void)
{
    delete_connections_table();
    unregister_netfilter_hooks();
    unregister_process();
    cfw_print_leak_detection();
    cfw_debug(DBG_INFO, "Colonel Firewall Unloaded");
}


module_init(cfw_init);
module_exit(cfw_exit);