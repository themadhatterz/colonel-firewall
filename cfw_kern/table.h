#ifndef __TABLE_H__
#define __TABLE_H__

#define MAX_KEY_SIZE 255

#define TBL_ERR 0
#define TBL_OK 1


struct entry {
    char *key;
    void *value;
    struct entry *next;
};

struct hash_table {
    size_t size;
    struct entry **entries;
};

int table_set(struct hash_table *table, const char *key, void *value);
unsigned int create_hash(struct hash_table *table, const char *key);
void table_free_entry(struct entry *tbl_entry);
void table_destroy(struct hash_table *table);
void table_delete(struct hash_table *table, const char *key);
void *table_get(struct hash_table *table, const char *key);
struct entry *table_create_entry(const char *key, void *value);
struct hash_table *table_create(size_t tablesize);




#endif // __TABLE_H__