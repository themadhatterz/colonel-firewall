#ifndef __CFW_H__
#define __CFW_H__

// Constants
#define PROCESS_NAME "cfw_kern"
#define PROCESS_PERM 0646
#define CFW_MAJOR 202

// IOCTL
#define IOCTL_SAY_HI _IO(CFW_MAJOR, 0)
#define IOCTL_CONN_TABLE _IO(CFW_MAJOR, 1)
#define IOCTL_DEBUG _IO(CFW_MAJOR, 2)

// Net Filter Hooks
static struct nf_hook_ops nfho_localin;
static struct nf_hook_ops nfho_localout;
static struct nf_hook_ops nfho_preroute;
static struct nf_hook_ops nfho_postroute;
static struct nf_hook_ops nfho_forward;
int register_netfilter_hooks(void);
void unregister_netfilter_hooks(void);

// Process Function
static struct proc_dir_entry *process_entry;
int register_process(void);
void unregister_process(void);
static ssize_t cfw_read(struct file *fp, char *buffer, size_t len, loff_t *offset);
static ssize_t cfw_write(struct file *fp, const char *buffer, size_t len, loff_t *offset);
long cfw_ioctl(struct file *fp, unsigned int ioctl_num, unsigned long ioctl_param);

// Colonel Firewall
unsigned int cfw_handle_packet(struct sk_buff *skb);
unsigned int cfw_inbound(void *private, struct sk_buff *skb, const struct nf_hook_state *state);
unsigned int cfw_outbound(void *private, struct sk_buff *skb, const struct nf_hook_state *state);
unsigned int cfw_forward(void *private, struct sk_buff *skb, const struct nf_hook_state *state);

#endif // __CFW_H__