#ifndef __CFWIS_H__
#define __CFWIS_H__

#include <linux/string.h>

// Debug Flags
#define DBG_ERROR 1U << 0
#define DBG_INFO  1U << 1
#define DBG_CONN  1U << 2
#define DBG_TABLE 1U << 3
#define DBG_INFRA 1U << 4

void *cfw_kmalloc(unsigned long int size);
void cfw_kfree(void *ptr);
void cfw_print_leak_detection(void);
void cfw_update_debug_flag(unsigned int flag);
int cfw_check_flag(unsigned int flag);

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define cfw_debug(FLAG, MESSAGE, ...) if (cfw_check_flag(FLAG)) pr_info("%s:%s:%d: " MESSAGE "\n", __FILENAME__, __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define update_debug_flags(FLAG) cfw_update_debug_flag(FLAG)


#endif // __CFWIS_H__