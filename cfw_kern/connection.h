#ifndef __CONNECTION_H__
#define __CONNECTION_H__


#define CONNECTIONS_TABLE_SIZE 10000

struct cfw_connection {
    unsigned char proto;
    unsigned int saddr;
    unsigned int daddr;
    unsigned short sport;
    unsigned short dport;
};

int init_connections_table(void);
int is_first_packet(struct tcphdr *tcp_header);
int is_existing_conn(struct iphdr *ip_header, u16 source_port, u16 destination_port);
int create_cfw_conn(struct iphdr *ip_header, u16 source_port, u16 destination_port);
void delete_connections_table(void);
void print_connections_table(void);


#endif // __CONNECTION_H__