#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>

#include "infra.h"


#ifdef LEAK
static unsigned long int total_running_bytes = 0;
#endif

// Debug Level : Default Flags (DBG_ERROR | DBG_INFO)
static unsigned int DBG_LEVEL = (DBG_ERROR | DBG_INFO);


int
cfw_check_flag(unsigned int flag)
{
    if (unlikely(DBG_LEVEL & flag))
    {
        return 1;
    }

    return 0;
}


void
cfw_update_debug_flag(unsigned int flag)
{
    DBG_LEVEL ^= flag;
}


void *
cfw_kmalloc(unsigned long int size)
{
    void *ptr = kmalloc(size, GFP_KERNEL);
    cfw_debug(DBG_INFRA, "Allocated pointer of size=%lu", sizeof(ptr));

#ifdef LEAK
    // This only tracks pointer to pointer free, not actual memory!
    total_running_bytes += sizeof(ptr);
#endif

    return ptr;
}

void
cfw_kfree(void *ptr)
{
#ifdef LEAK
    total_running_bytes -= sizeof(ptr);
#endif
    cfw_debug(DBG_INFRA, "Freeing pointer of size=%lu", sizeof(ptr));
    kfree(ptr);
}

void
cfw_print_leak_detection(void)
{
#ifdef LEAK
    cfw_debug(DBG_INFO, "total_running_bytes=%lu", total_running_bytes);
#endif
}