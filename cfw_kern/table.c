#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/string.h>

#include "table.h"
#include "infra.h"


// Hashing function for table operations
unsigned int
table_hash(struct hash_table *table, const char *key)
{
    unsigned int hash = 0;
    unsigned int i = 0;
    unsigned int key_len = strlen(key);

    for (i = 0; i < key_len; i++)
    {
        hash = hash * 37 + key[i];
    }

    hash = hash % table->size; // hash must fit inside table size

    cfw_debug(DBG_TABLE, "key=%s, hash=%u", key, hash);
    return hash;
}


// Free function for table entry
void
table_free_entry(struct entry *tbl_entry)
{
    struct entry *next = tbl_entry;

    while (tbl_entry != NULL)
    {
        if (tbl_entry->key) cfw_kfree(tbl_entry->key);
        if (tbl_entry->value) cfw_kfree(tbl_entry->value);

        next = tbl_entry->next;
        cfw_kfree(tbl_entry);
        tbl_entry = next;
    }
}


// Created table entry and assigns key and value
struct entry *
table_create_entry(const char *key, void *value)
{
    struct entry *tbl_entry = cfw_kmalloc(sizeof(struct entry));
    if (!tbl_entry)
    {
        cfw_debug(DBG_ERROR, "failed memory allocation for table entry");
        return NULL;
    }

    tbl_entry->key = cfw_kmalloc(strlen(key) + 1);

    if (!tbl_entry->key)
    {
        cfw_debug(DBG_ERROR, "failed memory allocation for entry key");
        table_free_entry(tbl_entry);
        return NULL;
    }

    strcpy(tbl_entry->key, key);
    tbl_entry->value = value;
    tbl_entry->next = NULL;

    return tbl_entry;
}


// Destroys table completely including all entries
void
table_destroy(struct hash_table *table)
{
    int i = 0;
    struct entry *tbl_entry = NULL;

    if (table)
    {
        for (i = 0; i < table->size; i++)
        {
            tbl_entry = table->entries[i];
            if (tbl_entry) table_free_entry(tbl_entry);
        }
        cfw_kfree(table->entries);
        cfw_kfree(table);
    }
}


// Creates table of given size
struct hash_table *
table_create(size_t tablesize)
{
    int i = 0;
    struct hash_table *table = cfw_kmalloc(sizeof(struct hash_table));

    if (!table)
    {
        cfw_debug(DBG_ERROR, "failed memory allocation for table");
        return NULL;
    }
    table->size = tablesize;
    table->entries = cfw_kmalloc((sizeof(struct entry *) * tablesize));

    if (!table->entries)
    {
        cfw_debug(DBG_ERROR, "failed memory alocation for table entries");
        table_destroy(table);
        return NULL;
    }

    for (i = 0; i < tablesize; i++)
    {
        table->entries[i] = NULL;
    }

    return table;
}


// Set new table entry in given table
int
table_set(struct hash_table *table, const char *key, void *value)
{
    unsigned int slot = table_hash(table, key);
    struct entry *prev = NULL;
    struct entry *tbl_entry = table->entries[slot];

    // Slot is empty
    if (tbl_entry == NULL)
    {
        cfw_debug(DBG_TABLE, "no existing entry for hash=%u", slot);
        table->entries[slot] = table_create_entry(key, value);
        if (!table->entries[slot])
        {
            cfw_debug(DBG_ERROR, "failed slot assignment for key=%s, hash=%u", key, slot);
            return TBL_ERR;
        }
        return TBL_OK;
    }

    // Hash collision
    while (tbl_entry != NULL)
    {
        cfw_debug(DBG_TABLE, "existing table entry, existing=%s, new=%s", tbl_entry->key, key);
        if (strcmp(tbl_entry->key, key) == 0)
        {
            cfw_debug(DBG_TABLE, "key match, replacing value");
            cfw_kfree(tbl_entry->value);
            tbl_entry->value = value;
            return TBL_OK;
        }
        cfw_debug(DBG_TABLE, "existing entry miss");
        prev = tbl_entry;
        tbl_entry = prev->next;
    }

    cfw_debug(DBG_TABLE, "no existing entry match for key=%s", key);
    prev->next = table_create_entry(key, value);
    if (!prev->next)
    {
        cfw_debug(DBG_ERROR, "failed link assignment for key=%s, hash=%u", key, slot);
        return TBL_ERR;
    }

    return TBL_OK;
}


// Gets void * value out of table for given key
void *
table_get(struct hash_table *table, const char *key)
{
    unsigned int slot = table_hash(table, key);
    struct entry *tbl_entry = table->entries[slot];

    cfw_debug(DBG_TABLE, "get for key=%s", key);
    if (tbl_entry == NULL)
    {
        cfw_debug(DBG_TABLE, "key not found");
        return NULL;
    }

    cfw_debug(DBG_TABLE, "existing entry for hash=%u", slot);
    while (tbl_entry != NULL)
    {
        cfw_debug(DBG_TABLE, "get=%s, existing=%s", key, tbl_entry->key);
        if (strncmp(tbl_entry->key, key, MAX_KEY_SIZE) == 0)
        {
            cfw_debug(DBG_TABLE, "key match, returning value");
            return tbl_entry->value;
        }

        cfw_debug(DBG_TABLE, "existing entry miss");
        tbl_entry = tbl_entry->next;
    }

    cfw_debug(DBG_TABLE, "no key=%s found in table", key);
    return NULL;
}


// Deletes table entry of given key
void
table_delete(struct hash_table *table, const char *key)
{
    unsigned int slot = table_hash(table, key);
    struct entry *tbl_entry = table->entries[slot];

    if (tbl_entry) table_free_entry(tbl_entry);
    cfw_debug(DBG_ERROR, "asked for deletion of non existing key=%s", key);
}